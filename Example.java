import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.Arrays;
public class Example {

	public static void main(String[] args) {    
		try
        {
			for(int n=1;n<=10;n++) {
				File file = new File(n+".in");
	        	InputStream inputStream = new FileInputStream(file);
	        	
	            Scanner scanner = new Scanner(inputStream);
	            String data =  scanner.nextLine();         
	            data = data.substring(1,data.length()-1);       
	            String[] A_String = data.split(",");   
	            int[] A = new int[A_String.length];
	            for(int i = 0; i < A_String.length; i++){
	                A[i] = Integer. parseInt(A_String[i]);
	            }
	            // In-place reversal of array
	            for(int i = 0; i < A.length / 2; i++){
	                // Swapping the elements
	                int j = A[i];
	                A[i] = A[A.length - i - 1];
	                A[A.length - i - 1] = j;
	            }

	            // Output
	            System.out.println("A = " + Arrays.toString(A));
	            
	            PrintWriter writer;
	    		try {
	    			writer = new PrintWriter(n+".out", "UTF-8");
	    			writer.print("[");
	    			for(int i=0;i<A.length-1;i++) {
	    				writer.print(A[i]+",");
	    			}
	    			writer.print(A[A.length-1]+"]");
	    			writer.close();
	    		} catch (FileNotFoundException e) {
	    			e.printStackTrace();
	    		} catch (UnsupportedEncodingException e) {
	    			e.printStackTrace();
	    		}
			} 
        } catch (Exception e)
        {
            e.printStackTrace ();
        }

	}

}
